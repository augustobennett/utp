package learning;

import java.io.IOException;

// Cola With Genrerics!
public class Cola <T>{
    private int max; // maxima capacidad
    private int pos = 0; // cursor para determinar posicion acutual
    private int dcursor = 0; // cursor para obtener dato de la cola
    T[] data;
    public Cola(int size){
        System.out.println( "La cola cuenta con capacidad  de " + size + " elementos.");
        data = (T[]) new Object[size];
        max = size;
    }

    public boolean isEmpty(){
            return pos == 0 || dcursor == max;
            //return pos == max;
    }

    public boolean isFull(){
        return dcursor == max || pos == max;
    }


    public void enqueue(T data){
        if ( !isFull()){  // si no esta lleno entonces puede encolar
            this.data[pos++] = data;
        }else{
            System.out.println("OVerflow!");
        }
    }

    public T dequeue(){
        if ( !isEmpty()) { // si no esta vacio entonces puedo decolar
            T temp = this.data[dcursor];
            this.data[dcursor++] = null;
            return temp;
        }else{
            System.out.println("Underflow!");
            return null;
        }
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for ( int i = data.length -1; i >= 0; i--){
            if (data[i] == null) continue;;
            sb.append("\t"+ data[i] + "\t");
        }
        sb.append("]");
        return sb.toString();
    }

    public void reset(){
        pos = 0; dcursor =0;
    }

}

class MainCola{
    public static void main(String... args) throws IOException {
        Cola<String> urls = new Cola<String>(3);
        urls.enqueue( "primero");
        urls.enqueue( "segundo");
        urls.enqueue( "ultimo");
        System.out.println( urls.toString());
        System.out.println( urls.dequeue());
        System.out.println( urls.toString());
        System.out.println( urls.dequeue());
        System.out.println( urls.toString());
    }
}
