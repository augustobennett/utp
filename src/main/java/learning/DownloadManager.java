package learning;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.*;

// clase DownloadManager
// Auhtor : Augusto Bennentt

// Every interface, class should be in its unique file
// aqui todo esta dentro de un archivo lo cual es mala practica xD.

interface IProgress{ // monitorear el progreso de forma mas eficiente
    void showProgress(ConcurrentHashMap<String, String> monitor);
}

public class DownloadManager implements IProgress {
    // clase abstracta que extenderan los archivos descargables
    static abstract class FileDownload implements Callable<Void> {
        private Logger log;
        private String urlFile;
        private String dirPath;
        private IProgress callback;
        private static final ConcurrentHashMap<String, String> monitor = new ConcurrentHashMap<>();
        private long ID;
        public FileDownload(Logger log, String dirPath, String urlFile, IProgress callback) {
            this.log = log;
            this.dirPath = dirPath;
            this.urlFile = urlFile;
            this.callback = callback;
            ID = System.currentTimeMillis() + new Random().nextInt(1000000); // Unique ID
        }
        @Override
        public Void call() throws Exception {
            if (urlFile == null) return null;
            log.writeLog("Begining downloading file.");
            String fileName = urlFile.substring( urlFile.lastIndexOf("/") + 1);
            HttpURLConnection conn = null;
            double bufferAcum =0;
                conn = (HttpURLConnection) new URL(urlFile).openConnection();
                long dataSize = conn.getContentLengthLong();
                try (BufferedInputStream in = new BufferedInputStream(conn.getInputStream());
                     FileOutputStream fileOutputStream = new FileOutputStream(dirPath + fileName)) {
                    byte dataBuffer[] = new byte[1024];
                    int bytesRead;
                    int resultado, temp;
                    while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                        bufferAcum += bytesRead;
                        fileOutputStream.write(dataBuffer, 0, bytesRead);
                        resultado = (int) (((double) bufferAcum / (double) dataSize) * 100.0);
                        monitor.put(String.valueOf(ID), "File -> " + fileName + " " + resultado +"%");
                        callback.showProgress(monitor);
                    }
                } catch (IOException e) {
                    monitor.put(String.valueOf(ID), "File -> " + fileName + " Error!");
                    callback.showProgress(monitor);
                    // handle exception
                }
            return null;
        }
    }

    // clase que representa un archivo descargable
    static class File2Download extends FileDownload{
        public File2Download(Logger log, String dirPath, String urlFile, IProgress callback) {
            super(log, dirPath, urlFile, callback); // esto no me gusta
        }

    }

    static interface Logger {
        void writeLog(String value); //
    }
    static class ConsoleLogger implements Logger {
        @Override
        public void writeLog(String value) {
            System.out.println(value);
        }
    }
    // main method
    public static void main(String... args) throws InterruptedException {
           new DownloadManager().beginMain(); // Best practice
    }


    public void beginMain() throws InterruptedException {
        //ConcurrentHashMap<String, String> monitor = new ConcurrentHashMap<>();
        Logger console = new ConsoleLogger(); // Logger Para mostrar recorrido
        // Logger File = new FileLogger() // por ejemplo para mostrar recorrido en un texto u otro lado
        File2Download file1 = new File2Download(console , "D:\\", "https://www.7-zip.org/a/7z1900-x64.msi", this);
        //File2Download file2 = new File2Download(console , "D:\\", "https://www.win-rar.com/fileadmin/winrar-versions/winrar-x64-602ar.exe", this);
        List<File2Download> archivosADescargar  = new ArrayList<>();
        archivosADescargar.add(file1);
       // archivosADescargar.add( file2);
        ExecutorService s = Executors.newFixedThreadPool(3);
        s.invokeAll(archivosADescargar); // los archivos se descargan concurrentemente
        s.shutdownNow(); //
    }


    @Override
    public void showProgress(ConcurrentHashMap<String, String> monitor) {
        // This is the method callback Monitor ! !
        try{
            //System.out.print("\033[H\033[2J");// borramos la pantalla // linux command
            //System.out.flush();
            //Runtime.getRuntime().exec("cls"); // windows clear command
            //new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            Runtime.getRuntime().exec("cls");
        }catch (IOException  e ){
            // no podemos borrar la pantalla xD ?
        }
        //System.out.println( monitor.size());
        System.out.println("=========================================");
        for ( Map.Entry<String, String> v : monitor.entrySet()){
             System.out.println( String.format("|  %s           |", v.getValue()));
        }
        System.out.println("=========================================");
    }
}

