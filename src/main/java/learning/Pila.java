package learning;

//Las pilas Stacks
// Author Augusto Bennett
class Pila<T>{
    private T[] data ;
    private int tope = 0;
    private int max;
    public Pila(int size){
        data = (T[]) new Object[size];
        max = size ;
    }
    // insertar la data
    public void push( T data)  {
        if ( !isFull()){ // si no esta lleno entonces se inserta la data
            this.data[tope++] = data; // se asigna primero tope, luego aumenta
        }else{
            System.out.println("Overflow!");
        }
    }
    public boolean isEmpty(){
        return tope == 0;
    }
    public int getTope(){
        return this.tope ;
    }
    public boolean isFull(){
        return tope == max;
    }
    // obtener la data
    public T pop( ){
        if ( !isEmpty()){ // si no esta vacio entonces obtiene un dato
            T temp = this.data[--tope];
            this.data[tope] = null;
            return temp; // se diminuye tope, luego se asigna
        }else{
            //System.out.println("Underflow!");
            return null;
        }
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for ( int i = data.length -1; i >= 0; i--){
            if (data[i] == null) continue;;
            sb.append("\t"+ data[i] + "\t");
        }
        sb.append("]");
        return sb.toString();
    }

}

class MainPila{
    public static void main(String... args)  {
        Pila<String> urls = new Pila<String>(3);
        urls.push( "primero");
        urls.push( "segundo");
        urls.push( "ultimo");
        System.out.println( urls.toString());
        System.out.println( urls.pop());
        System.out.println( urls.toString());
        System.out.println( urls.pop());
        System.out.println( urls.toString());
    }
}