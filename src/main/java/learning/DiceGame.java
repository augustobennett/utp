package learning;
import javax.swing.JOptionPane;

// Auhtor: Augusto Ennett
// class Dice
class Dice {
    public Dice(){    }
    public Dice( boolean infinite ){
        this.infinite  = infinite;
    }
    private int amount = 0;
    private Boolean infinite = false    ;
    private int[][][] faces = {{{0, 0, 0}, {0, 1, 0}, {0, 0, 0}},
            {{1, 0, 0}, {0, 0, 0}, {0, 0, 1}},
            {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}},
            {{1, 0, 1}, {0, 0, 0}, {1, 0, 1}},
            {{1, 0, 1}, {0, 1, 0}, {1, 0, 1}},
            {{1, 0, 1}, {1, 0, 1}, {1, 0, 1}}};

    public void rollDice() {
        amount = (int) (Math.random() * 6);
        showDice();
    }

    private  void showDice() {
        StringBuilder sb = new StringBuilder();
        for (int j = 0; j < 3; j++) {
            System.out.print("| ");
            sb.append("|");
            for (int k = 0; k < 3; k++) {
                sb.append(String.format("%3s", faces[amount][j][k] == 1 ? "O" : "     "));
                System.out.print(faces[amount][j][k] == 1 ? "O" : " ");
                System.out.print(" ");
            }
            sb.append(String.format("%3s", "|"));
            sb.append(String.format("%3s", "\n"));
            System.out.print("|");
            System.out.print("\n");
        }
        JOptionPane.showMessageDialog(null, sb.toString());
        if ( infinite) rollDice();
    }
}

// Main class
public class DiceGame {
    public static void main(String... args){
        Dice dado = new Dice(true);
        dado.rollDice();
    }
}
