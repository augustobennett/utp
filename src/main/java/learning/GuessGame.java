package learning;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
// Auhor : Augusto Bennett
// xD
public class GuessGame {
    private static String palabra = "computadora";
    private static String hide;
    private static final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    private static boolean Running = true;

    public static void main(String... args) throws IOException {
        generateHide();
        do{
            System.out.println("Inserte un aletra : ");
            getLetter(br.readLine());
            System.out.println(hide);
        }while ( Running );
    }
    // ocultar la palabra
    public static void generateHide() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < palabra.length(); i++) {
            sb.append("_");
        }
        hide = sb.toString();
    }
    // obtener la palabra de la consola y comprobar
    public static void getLetter(String i) {
        if (checkWin(i)) {
            System.out.println("You have guessed the word!");
            hide = i;
            Running = !Running;
            return;
        }
        if (palabra.contains(i) && i.length() == 1) {
            char[] iChars = i.toCharArray();
            char[] hChars = hide.toCharArray();
            char[] chars = palabra.toCharArray();
            for (int j = 0; j < chars.length; j++) {
                if (chars[j] == iChars[0]) {
                    hChars[j] = iChars[0];
                }
            }
            hide = new String(hChars);
            // esto puede ser | Running = checkWin( hide );
            if ( checkWin(hide)){
                System.out.println("you have won!");
                Running = !Running;
            }
        } else if (i.length() >= 2) {
            System.out.println("Solamente se acep un valor");
        }else{
            System.out.println("Su letra no se encuentra dentro de la palabra");
        }
    }
    // comprubea si la palabra completa ah sido adiviniada
    public static boolean checkWin(String word) {
        return palabra.contains(word) && palabra.equals(word) && palabra.length() == word.length();
    }

}
