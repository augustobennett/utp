package learning;

import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;

// Author : AUgusto Bennett
// Lista enalazadas con cabezera y elementos disponibles
class ListaEnlaze<T> {
    protected int size;
    protected Node<T> Head;
    protected Node<T> cursor;
    protected Pila<Node<T>> avails; // creamos una pila de elementos disponibles

    // creamos una clase interna, para los nodos.
    protected class Node<T> {
        private T data;
        private Node next;

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }

    // Size: equivale a la cantidad de cache que puede tener los elementos disponibles en cada momento
    // no es necesario para el linkedLista. ya que se puede agregar infinitamente a ella.
    public ListaEnlaze(int size) {
        if (size <= 0)
            throw new RuntimeException("Size must be gretaer than 0");
        this.size = size;
        avails = new Pila<>(size);
    }

    public ListaEnlaze() {
        this(100); // 100 elementos de disponibilidad
    }

    public void addData(T data) {
        // sin este código la complejida de insercion seria de O(n) en uno de los nodos que su valor a sido borrado.
        if (!avails.isEmpty()) {
            Node<T> temp = avails.pop(); // Obtenemos un nodo que ha sido borrado, "esta disponible"
            temp.setData(data); // le establecemos el valor
            return; // terminamos.   Complejidad O(1),
        }
        // La complejidad de inserción al final es de O(1)
        Node<T> node = new Node<>();
        node.setData(data);
        if (Head == null) {
            Head = node;
            cursor = node;
        } else {
            cursor.setNext(node);
            cursor = node;
        }
    }

    // Cache data
    public boolean deleteData(T data) {
        Node<T> lastNode = cursor; // guardamos la posicion final de cursor
        cursor = Head; // comenzamos desde la cabezara
        // buscamos el valor que queremos borrar secuencialmente
        do {
            T temp = cursor.getData();
            if (temp == data) {
                // found something
                cursor.setData(null); // delete the value ; "borramos el valor", en realidad solo se pone a Nulo
                avails.push(cursor); // introducimos nodo con valor NUlo a la pila; osea el valor ha sido borrado
                cursor = lastNode;  // reset the node to the last one; // volvemos cursor a su posicion final
                return true;
            }
        } while ((cursor = cursor.getNext()) != null);
        return false; // no se encontro data
    }

    protected void showValues() {
        cursor = Head;
        do {
            System.out.printf(" %s -> ", cursor.getData());
        } while ((cursor = cursor.getNext()) != null);
    }
}


// Author : Augsuto BEnnett
// Lista enlazada pero con elementos ordenados solamente OOP
// Todo aquello que reciba la clase tiene que implementar la interfaz comparable para poder hacer las comparaciones.
// disponibilidad no funciona por ahora... xD
class SortedListaEnlaze<T extends Comparable<T>> extends ListaEnlaze<T> {
    public SortedListaEnlaze() {
        super();
    }

    public SortedListaEnlaze(int size) {
        super(size);
    }

    @Override
    public void addData(T data) {
        Node<T> nodo = new Node<T>();
        nodo.setData(data);
        if (Head == null) {
            Head = nodo;
            cursor = nodo;
        } else {
            if (data.compareTo(Head.getData()) <= 0) { // el valor es menor al primero, ira al inicio
                nodo.setNext(Head);
                Head = nodo;
            } else if (data.compareTo(cursor.getData()) > 0) { // valor es mayor al ultimo, ira al final
                cursor.setNext(nodo);
                cursor = nodo;
            } else { // valor estara en medio de algunos de los nodos
                Node<T> lastElement = cursor;
                cursor = Head;
                while (cursor.getNext() != null &&   data.compareTo(cursor.getNext().getData()) < 0) {
                    cursor = cursor.getNext();
                 } 
                }
            }
        }
    }
}

// clase tutorial
public class LnkList {
    public static void main(String... args) throws IOException {
        System.out.println("\n=================");
        ListaEnlaze<String> cadenas = new ListaEnlaze<>(5);
        cadenas.addData("augusto");
        cadenas.addData("Bennett");
        cadenas.addData("Kios");
        cadenas.addData("Rober");
        cadenas.addData("Marcos");
        cadenas.deleteData("Rober");
        cadenas.deleteData("Marcos");
        cadenas.addData("angel");
        cadenas.showValues();
        System.out.println("\n=================");
        SortedListaEnlaze<Character> a = new SortedListaEnlaze<>();
        a.addData('B');
        a.addData('H');
        a.addData('G');
        a.addData('A');
        a.addData('E');
        a.showValues();
    }
}
