package labs.practica1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
/*
    Buscar el area de un triangulo ?
 */
public class Lab3 {
    public static void main(String... args) throws IOException {
        double base, area, resultado, altura;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        // leer altura
        System.out.println("Introduzca la altura: ");
        altura = Double.parseDouble( br.readLine());
        // leer base
        System.out.println("Intoduce la base: ");
        base  = Double.parseDouble(br.readLine());
        area = (0.5) * base * altura;
        // calcular area de triangulo
        System.out.printf("El area del triangulo es : %.2f", area);
    }
}
