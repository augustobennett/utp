package labs.practica1;
import javax.swing.JOptionPane;
/*
	Un alumno desea saber cuál será su calificación final en la materia de Algoritmos.
    Dicha calificación se compone de los siguientes porcentajes:
•	55% del promedio de sus tres calificaciones parciales.
•	30% de la calificación del examen final.
•	15% de la calificación de un trabajo final
[JOptionPane]
 */
// Class lab1 UTP
public class Lab1 {
    public static void main(String... args){
        Double notaExamen = 0.0, totalNotas = 0.0, trabajoFinal = 0.0;
        // pedir las tres notas
        for ( int i = 0 ; i < 3; i ++){
            String resultado = JOptionPane.showInputDialog(null, "Inserte nota #" + (i+1));
            totalNotas += Double.parseDouble(resultado);;
        }
        // calcular el 55% de las 3 notas
        totalNotas = (totalNotas/ 3) * .55;
        String examen = JOptionPane.showInputDialog(null, "Nota de examen : ");
        // calcular el 30% de la nota de examen
        notaExamen = Double.parseDouble(examen) * .30;
        String tFinal = JOptionPane.showInputDialog(null, "Nota trabajo final : ");
        // calcular el 15% de la nota del trabajo final
        trabajoFinal = Double.parseDouble(tFinal) * .15;
        double calificacionFinal = totalNotas + notaExamen + trabajoFinal;
        JOptionPane.showMessageDialog(null, String.format("La nota final es de %.2f", calificacionFinal));
    }
}
