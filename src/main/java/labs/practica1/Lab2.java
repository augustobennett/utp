package labs.practica1;
import javax.swing.JOptionPane;
/*
Leer dos números, calcular e imprimir el valor de suma, resta, producto y su división [JOptionPane].
 */
public class Lab2 {
    public static void main(String... args){
        Double num1, num2; // declaracion de dos numeros
        // pedir numeros
        String numero1 = JOptionPane.showInputDialog(null, "Introduzca el primer numero : ");
        String numero2 = JOptionPane.showInputDialog(null, "Introduzca el segundo numero : ");
        // transofrmar los valores introducidos a numeros decimales
        num1 = Double.parseDouble(numero1);
        num2 = Double.parseDouble(numero2);
        // Formatear la cadena de salida
        String resultadoFinal = String.format("La suma es de : %.2f \n La resta es de : %.2f  \n la multiplicacion es de : %.2f \n la division es de: %.2f",
                (num1+num2),(num1-num2), (num1*num2), (num1/num2) );
        JOptionPane.showMessageDialog(null, resultadoFinal);
    }
}
