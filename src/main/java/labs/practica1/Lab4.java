package labs.practica1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
Leer dos números, calcular e imprimir, el valor de la suma, resta, multiplicación, división y media aritmética.
[BufferedReader]
 */
public class Lab4 {
    public static void main(String... args)throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        double num1, num2;
        System.out.println("Introduzca primer numero : ");
        num1 = Double.parseDouble(br.readLine());
        System.out.println("Introduzca segundo numero : ");
        num2 = Double.parseDouble(br.readLine());
        String resultado = String.format("La suma es de : %.2f \nLa resta es de : %.2f  \nla multiplicacion es de : %.2f \nla division es de: %.2f" +
                        "\nLa media es de : %.2f",
                (num1+num2),(num1-num2), (num1*num2), (num1/num2), ((num1+num2)/2));
        System.out.println( "=============================");
        System.out.println(resultado);
        System.out.println( "=============================");
    }
}
