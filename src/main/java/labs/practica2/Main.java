package labs.practica2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    private BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    public static void main(String... args) throws IOException {
        Main m = new Main();
        m.mainAlumno(); // Main para alumno
        m.mainPersona(); // Main para Persona
        m.mainRectangulo(); // Main para Rectangulo
    }
    public void mainAlumno() throws IOException {
        System.out.println("=== Practica 1 ====== \n ==== Alumnos ======");
        Alumno a1 = new Alumno();
        double nota1, nota2, nota3;
        System.out.println("Introduzca nota 1: ");
        nota1 = Double.parseDouble(br.readLine());
        System.out.println("Introduzca nota 2: ");
        nota2 = Double.parseDouble(br.readLine());
        System.out.println("Introduzca nota 3: ");
        nota3 = Double.parseDouble(br.readLine());
        a1.asignar(nota1, nota2, nota3);
        System.out.println("El resultado es de: " + a1.calcular());
    }
    public void mainPersona() throws IOException {
        Persona p = new Persona();

    }
    public void mainRectangulo() throws IOException {
        Rectangulo r = new Rectangulo();
    }
}
