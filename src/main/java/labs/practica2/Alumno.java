package labs.practica2;

public class Alumno {
    private double nota1, nota2, nota3;
    public Alumno(){
       nota2 = nota1 = nota3 = 0;
    }
    public void asignar( double nota1, double nota2, double nota3){
        this.nota1 = nota1;
        this.nota2 = nota2;
        this.nota3 = nota3;
    }

    public Double calcular(){
        return ( nota1 + nota2 + nota3 ) / 3;
    }
}
